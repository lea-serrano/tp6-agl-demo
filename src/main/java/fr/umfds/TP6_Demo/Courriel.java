package fr.umfds.TP6_Demo;

public class Courriel {
	
	private String adresseElectroniqueDestination;
	private String titreMessage;
	private String corpsMessage;
	private String piecesJointes;
	
	public Courriel (String adresse, String titre, String corps, String pieces) {
		adresseElectroniqueDestination = adresse;
		titreMessage = titre;
		corpsMessage = corps;
		piecesJointes = pieces;
	}
	
	public boolean envoyer() {
		boolean adresseValide = false;
		boolean titreValide = false;
		boolean corpsValide = false;
		
		
		if (adresseElectroniqueDestination.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$")) {
			adresseValide = true;
		}
	
		
		if (titreMessage != null) {
			titreValide = true;
		}
		
		
		if (corpsMessage.indexOf("PJ")>0 || corpsMessage.indexOf("joint")>0 || corpsMessage.indexOf("jointe")>0) {
			if (piecesJointes != null) {
				corpsValide = true;
			}
		}
		else {
			if (piecesJointes == null) {
				corpsValide = true;
			}
		}
		return adresseValide&&titreValide&&corpsValide;
	}

}