package fr.umfds.TP6_Demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestColis {

private static float tolerancePrix=0.001f;
	
	static Colis colis1;
	
	//Initialisation des objets Colis à tester dans cette classe
	@BeforeAll
	static void initAll() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 10, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si l'affichage de la méthode souhaité est le même que celui de la méthode toString()*/
	@Test
	public void testToString() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement - 3.5f est bien inférieur à tolerancePrix*/
	@Test
	public void testAffranchissement() {
		assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur du remboursement - 100.0f est bien inférieur à tolerancePrix*/
	@Test
	public void testTarifRemboursement() {
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de getDeclareContenu est bien égal à "train electrique"*/
	@Test
	public void testDeclareContenu() {
		assertEquals("train electrique", colis1.getDeclareContenu());
	}
	
}
