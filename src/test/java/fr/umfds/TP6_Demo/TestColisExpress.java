package fr.umfds.TP6_Demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestColisExpress {
	
static ColisExpress colis1;
	
//Initialisation des objets ColisExpress à tester dans cette classe
	@BeforeAll
	static void initAll() throws ColisExpressInvalide {
		colis1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", 
				"7877", 10, 0.02f, Recommandation.deux, "train electrique", 200, true);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si l'affichage de la méthode souhaité est le même que celui de la méthode toString()*/
	@Test
	public void testToString() {
		assertEquals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/10.0/0", colis1.toString());
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement est bien égal à 33f*/
	@Test
	public void testAffranchissement() {
		assertEquals(33f, colis1.tarifAffranchissement());
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si on a bien une levée d'exception de la classe ColisExpressInvalide lorsque
	l'on a un colis express de plus de 30kg (ici on teste avec 100kg)*/
	@Test
	public void testCreationColisAvecPoidsInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			new ColisExpress ("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 100, 0.02f, Recommandation.deux, "train electrique", 200);
		});
	}

}