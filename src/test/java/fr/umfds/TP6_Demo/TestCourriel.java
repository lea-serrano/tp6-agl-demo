package fr.umfds.TP6_Demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class TestCourriel {

	static Courriel c1;
	static Courriel c2;
	static Courriel c3;
	static Courriel c4;
	static Courriel c5;
	static Courriel c6;
	
	//Initialisation des objets Courriel à tester dans cette classe
	@BeforeAll
	static void initAll() {
		c1 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c2 = new Courriel("tototiti.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c3 = new Courriel("toto@titi.fr", null, "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c4 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", null);
		c5 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo!", null);
		c6 = new Courriel(".....", "Salut!", "Bonjour, comment ça va ? Voici une photo!", null);
	}
	
	/*Test qui vérifie un cas qui marche : 
	On n'a pas de valeurs nulles et on a bien une pièce jointe avec "PJ" dans le corps du message*/
	@Test
	public void testEnvoyerValide() {
		assertTrue(c1.envoyer());
	}
	
	/*Test qui vérifie un cas qui ne marche pas :
	On a une adresse mail qui est mal formée*/
	@Test
	public void testEnvoyerAdresseMailNonValide1() {
		assertFalse(c2.envoyer());
	}
	
	/*Test qui vérifie un cas qui ne marche pas :
	On a un titre de message qui est null*/
	@Test
	public void testEnvoyerTitreNonValide() {
		assertFalse(c3.envoyer());
	}
	
	/*Test qui vérifie un cas qui ne marche pas :
	On a "PJ" dans le texte mais pas de pièce jointe*/
	@Test
	public void testEnvoyerPJNonValide() {
		assertFalse(c4.envoyer());
	}
	
	/*Test qui vérifie un cas qui marche : 
	On n'a pas de piece jointe mais pas "PJ", "joint" ou "jointe" dans le corps du message*/
	@Test
	public void testEnvoyerSansPJValide() {
		assertTrue(c5.envoyer());
	}
	
	/*Test qui vérifie un cas qui ne marche pas :
	On a une adresse mail non valide*/
	@Test
	public void testEnvoyerAdresseMailNonValide2() {
		assertFalse(c6.envoyer());
	}
	
	/*Test paramétré qui vérifie des cas qui ne marchent pas :
	On a des adresses mail invalides*/
	@ParameterizedTest
	@ValueSource (strings = {"aaa", "....", "heho@test", "...@..", "####@###"})
	public void testAdresseNonValideAvecParametre(String address) {
		Courriel c = new Courriel(address, "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		assertFalse(c.envoyer());
	}
	
	/*Test, paramétré avec utilisation d'une autre méthode, qui vérifie des cas qui ne marchent pas et renvoie la raison :
	On a des adresses mail invalides
	Les elements qui seront testes en tant que variables adress et raison seront toutes les couples que renvoie 
	la méthode adresseIncorrecteEtRaison*/
	@ParameterizedTest(name = "({0}, {1})")
	@MethodSource ("addresseIncorrecteEtRaison")
	public void testAdresseNonValideAvecParametre2(String address, String raison) {
		Courriel c = new Courriel(address, "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		assertFalse(c.envoyer());
	}
	
	static Stream<Arguments> addresseIncorrecteEtRaison() {
		return Stream.of(
				Arguments.of("????", "caractereInvalide"),
				Arguments.of("####@", "adresseInvalide"),
				Arguments.of(" ", "adresseVide"));
	}
}