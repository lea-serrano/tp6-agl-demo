package fr.umfds.TP6_Demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestLettre {

	private static float tolerancePrix=0.001f;
	
	static Lettre lettre1;
	static Lettre lettre2;
	
	//Initialisation des objets Lettre à tester dans cette classe
	@BeforeAll
	static void initAll() {
		lettre1 = new Lettre("Le pere Noel", "famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel", "famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si l'affichage de la méthode souhaité est le même que celui de la méthode toString()*/
	@Test
	public void testToString1() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si l'affichage de la méthode souhaité est le même que celui de la méthode toString()*/
	@Test
	public void testToString2() {
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence", lettre2.toString());
	}
	
	/*Test qui vérifie un cas qui ne marche pas :
	On teste si l'affichage de la méthode souhaité n'est pas le même que celui de la méthode toString()*/
	@Test
	public void testToString3() {
		assertNotEquals(" ", lettre2.toString());
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement - 1.0f est bien inférieur à tolerancePrix*/
	@Test
	public void testAffranchissement1() {
		assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement - 2.3f est bien inférieur à tolerancePrix*/
	@Test
	public void testAffranchissement2() {
		assertTrue(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement - 1.5f est bien inférieur à tolerancePrix*/
	@Test
	public void testTarifRemboursement1() {
		assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur de l'affranchissement - 15.0f est bien inférieur à tolerancePrix*/
	@Test
	public void testTarifRemboursement2() {
		assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur du remboursement est bien égale à 1.5f*/
	@Test
	public void testTarifRemboursement3() {
		assertEquals(lettre1.tarifRemboursement(), 1.5f);
	}
	
	/*Test qui vérifie un cas qui marche :
	On teste si la valeur du remboursement est bien égale à 15f*/
	@Test
	public void testTarifRemboursement4() {
		assertEquals(lettre2.tarifRemboursement(), 15f);
	}

}
